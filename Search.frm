VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Search 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   0  'None
   Caption         =   "数据查询"
   ClientHeight    =   11772
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   18336
   LinkTopic       =   "Form5"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   11775
   ScaleMode       =   0  'User
   ScaleWidth      =   18455.92
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command4 
      Caption         =   "按条码查询"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.8
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11400
      TabIndex        =   11
      Top             =   1920
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "导出"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.8
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   12840
      TabIndex        =   10
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "检索结果(Retrieval result)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.4
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   1
      Left            =   0
      TabIndex        =   8
      Top             =   3120
      Width           =   14775
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
         Height          =   4095
         Left            =   -120
         TabIndex        =   9
         Top             =   360
         Width           =   14775
         _ExtentX        =   26056
         _ExtentY        =   7218
         _Version        =   393216
         BackColorBkg    =   12632256
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "返回"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.8
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   12840
      TabIndex        =   7
      Top             =   960
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "按时间查询"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.8
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11400
      TabIndex        =   6
      Top             =   960
      Width           =   1335
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2350
      _ExtentY        =   593
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "检索条件（Search condition）"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2295
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   600
      Width           =   11295
      Begin VB.Frame Frame1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "按条码"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   9
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1725
         Index           =   3
         Left            =   5160
         TabIndex        =   16
         Top             =   360
         Width           =   6015
         Begin VB.TextBox Text1 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   15
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   240
            TabIndex        =   17
            Text            =   "LN0084502852358520100004"
            Top             =   720
            Width           =   5535
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "查询工位"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   9
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1695
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   1695
         Begin VB.OptionButton Option3 
            BackColor       =   &H00C0C0C0&
            Caption         =   "转向器"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.8
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   360
            TabIndex        =   15
            Top             =   1080
            Width           =   1095
         End
         Begin VB.OptionButton Option2 
            BackColor       =   &H00C0C0C0&
            Caption         =   "稳定杆"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.8
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   360
            TabIndex        =   14
            Top             =   720
            Width           =   1095
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00C0C0C0&
            Caption         =   "摆臂"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.8
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   360
            TabIndex        =   13
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "结束时间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   9
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   770
         Index           =   5
         Left            =   2040
         TabIndex        =   3
         Top             =   1285
         Width           =   2775
         Begin MSComCtl2.DTPicker DTPicker2 
            Height          =   375
            Left            =   360
            TabIndex        =   4
            Top             =   285
            Width           =   2175
            _ExtentX        =   3831
            _ExtentY        =   656
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   79429633
            CurrentDate     =   42568
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "起始时间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   9
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   770
         Index           =   4
         Left            =   2040
         TabIndex        =   2
         Top             =   360
         Width           =   2775
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   375
            Left            =   360
            TabIndex        =   5
            Top             =   285
            Width           =   2175
            _ExtentX        =   3831
            _ExtentY        =   656
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   79429633
            CurrentDate     =   42370
         End
      End
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "数 据 查 询"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   18
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Index           =   0
      Left            =   6720
      TabIndex        =   0
      Top             =   120
      Width           =   2115
   End
End
Attribute VB_Name = "Search"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'Dim zl As Excel.Application
'Dim zlw As Excel.Workbook
'Dim zls As Excel.Worksheet
'Dim str As String, Execlname As String
Dim bool1 As Boolean
Dim StrChaxunName As String




Private Sub Command1_Click()
'单击导出按钮
    Dim i, j As Long
    Dim m, n As Long
    Dim DataArray()     As String
    Dim strRange     As String

    If MSHFlexGrid1.Text = "" Then
        MsgBox "无数据，请先查询数据！", vbInformation + vbOKOnly, "提示"
        Exit Sub
    End If
    
    m = MSHFlexGrid1.Cols        '电子表格的列数
    n = MSHFlexGrid1.Rows        '电子表格的行数
    If n > 65000 Or m > 200 Then
        MsgBox "对不起，该数据量已经超出了Excel表格最大容纳量，请缩减内容后再导出！", vbCritical + vbOKOnly, "提示"
        Exit Sub
    End If
    ReDim DataArray(n - 1, m - 1)  '确定动态数组的大小
        For i = 1 To n
            For j = 1 To m - 1
                DataArray(i - 1, j - 1) = MSHFlexGrid1.TextMatrix(i - 1, j)
            Next j
        Next i
    Set zl = CreateObject("Excel.Application")
    Set zlw = zl.Workbooks.Add
    Set zls = zlw.Worksheets("sheet1")
    
    zl.Visible = False
    
    With zls
        strRange = "A1: " & Chr(65 + m - 1) & "1 "                         '65   -->   A
        .Range(strRange).Merge                                             '合并excel第一行
'        .Range(strRange).HorizontalAlignment = xlCenter                    '居中显示
'        .Range(strRange).VerticalAlignment = xlCenter
'        .Range(Cells(1, 1), Cells(1, i)).Merge '合并单元格
        .Cells(1, 1).Font.Size = 20                                        '设置字体大小
        .Cells(1, 1).Font.Name = "黑体"                                    '设置字体
        .Cells(1, 1).Font.Bold = True                                      '字体加粗
        .Cells(1, 1) = StrChaxunName                                       '内容
    End With
    zls.Range("A2").Resize(n, m).Value = DataArray                         '导入数据
'    zlw.SaveAs (App.Path + "\Data\" + CStr(Now) + " " + "查询数据" + ".xlsx")
    zls.Cells.EntireColumn.AutoFit                                         '自动调整列宽
'    zls.Cells.HorizontalAlignment = xlCenter                               '居中显示
'    zls.Cells.VerticalAlignment = xlCenter
    zl.Visible = True                                                      '显示
'    zls.PrintPreview                                                       '打印预览
'    Set zls = Nothing
'    Set zlw = Nothing
'    zl.Quit
'    Set zl = Nothing
End Sub

Private Sub Command2_Click()
'单击返回
    Unload Me
    Main.Show
End Sub

Private Sub Command3_Click()
    Dim rs As ADODB.Recordset
    Dim str() As String, str1 As String
    Dim i As Long
    Dim number As Double, Number1 As Double, Number2 As Double, Number3 As Double, Number4 As Double, Number5 As Double, number6 As Double, number7 As Double
    Dim SQL As String, t As String
    Dim Date1 As Date, Date2 As Date
    Dim strDb As String
    MSHFlexGrid1.Clear

    If DTPicker1.Value > DTPicker2.Value Then
        MsgBox "请选择正确的检索时间！", vbOKOnly, "系统提示"
        DTPicker2.SetFocus
        Exit Sub
    End If
    Date1 = CDate(DTPicker1.Value)
    Date2 = CDate(DTPicker2.Value)
    Date1 = Date1 & " 00:00:00"
    Date2 = Date2 & " 23:59:59"

    Conn = App.Path & "\Data\测试数据.mdb"
    strDb = Conn
    Conn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
     strDb & ";Persist Security Info=False"
     t = Conn

    Adodc1.ConnectionString = Conn
    Adodc1.CommandType = adCmdText
    Adodc1.RecordSource = "SELECT * FROM 测试数据 WHERE 日期 between #" & Date1 & "# and #" & Date2 & "#  order by 序号"
    Adodc1.Refresh
    Set MSHFlexGrid1.DataSource = Adodc1
    
    MSHFlexGrid1.Refresh
    With MSHFlexGrid1
        .ColWidth(0) = 0
        .ColWidth(1) = 2000  '序号
        .ColWidth(2) = 3500 '日期
        .ColWidth(3) = 0 '用户名
        .ColWidth(4) = 1500  '型号
        .ColWidth(5) = 4100 '零件条码
        .ColWidth(6) = 1600 '外倾角
        .ColWidth(7) = 1600 '检测结果
        .ColWidth(8) = 0 '1#位移传感器数值
        .ColWidth(9) = 0 '1#位移传感器数值
        .ColWidth(10) = 0 '1#位移传感器数值

        .ColAlignmentFixed = 4
        For i = 0 To 10
            .ColAlignment(i) = 4
        Next i
    End With

    If MSHFlexGrid1.Rows = 1 Then
        Exit Sub
    Else
        For i = 1 To MSHFlexGrid1.Rows - 1
            MSHFlexGrid1.TextMatrix(i, 2) = Format(MSHFlexGrid1.TextMatrix(i, 2), "YYYY-MM-DD HH:MM:SS")
        Next
        MSHFlexGrid1.TopRow = MSHFlexGrid1.Rows - 1
'        Label3.Caption = MSHFlexGrid3.Rows - 1
    End If
End Sub







Private Sub Command4_Click()
'单击按日期数据查询按钮
    Dim rs As ADODB.Recordset
    Dim str() As String, str1 As String
    Dim i As Long
    Dim number As Double, Number1 As Double, Number2 As Double, Number3 As Double, Number4 As Double, Number5 As Double, number6 As Double, number7 As Double
    Dim SQL As String, t As String
    Dim Date1 As Date, Date2 As Date
'    If Option1.Value = False And Option2.Value = False And Option3.Value = False Then
'        MsgBox "请选择需要查询岗位"
'    End If
'    MSHFlexGrid1.Clear
'    If DTPicker1.Value > DTPicker2.Value Then
'        MsgBox "请选择正确的检索时间！", vbOKOnly, "系统提示"
'        DTPicker2.SetFocus
'        Exit Sub
'    End If
'    Date1 = CDate(DTPicker1.Value)
'    Date2 = CDate(DTPicker2.Value)
'    Date1 = Date1 & " 00:00:00"
'    Date2 = Date2 & " 23:59:59"

    Conn = App.Path & "\Data\测试数据.mdb"
    strDb = Conn
    Conn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
     strDb & ";Persist Security Info=False"
    t = Conn
    Adodc1.ConnectionString = Conn
    Adodc1.CommandType = adCmdText
    
     Adodc1.RecordSource = "SELECT * FROM 测试数据  WHERE 零件条码= '" & Text1.Text & "'"
    
'    If Option1.Value = True Then
''        Adodc1.RecordSource = "SELECT * FROM 副车架B线摆臂 WHERE 日期 between #" & Date1 & "# and #" & Date2 & "# order by 序号"
'        Adodc1.RecordSource = "SELECT * FROM 副车架B线摆臂 WHERE 零件条码= '" & Text1.Text & "'"
'    ElseIf Option2.Value = True Then
'        Adodc1.RecordSource = "SELECT * FROM 副车架B线稳定杆 WHERE 零件条码= '" & Text1.Text & "'"
'    ElseIf Option3.Value = True Then
'        Adodc1.RecordSource = "SELECT * FROM 副车架B线转向器 WHERE 零件条码= '" & Text1.Text & "'"
'    End If
    Adodc1.Refresh
'    Label2.Caption = Adodc1.Recordset.RecordCount

    Set MSHFlexGrid1.DataSource = Adodc1
    MSHFlexGrid1.Refresh
    With MSHFlexGrid1
        .ColWidth(0) = 0
        .ColWidth(1) = 2000  '序号
        .ColWidth(2) = 3500 '日期
        .ColWidth(3) = 0 '用户名
        .ColWidth(4) = 1500  '型号
        .ColWidth(5) = 4800 '零件条码
        .ColWidth(6) = 1600 '外倾角
        .ColWidth(7) = 1600 '检测结果
        .ColWidth(8) = 0 '1#位移传感器数值
        .ColWidth(9) = 0 '1#位移传感器数值
        .ColWidth(10) = 0 '1#位移传感器数值

        .ColAlignmentFixed = 4
        For i = 0 To 10
            .ColAlignment(i) = 4
        Next i
    End With

    If MSHFlexGrid1.Rows = 1 Then
        Exit Sub
    Else
        For i = 1 To MSHFlexGrid1.Rows - 1
            MSHFlexGrid1.TextMatrix(i, 2) = Format(MSHFlexGrid1.TextMatrix(i, 2), "YYYY-MM-DD HH:MM:SS")
        Next
        MSHFlexGrid1.TopRow = MSHFlexGrid1.Rows - 1
'        Label3.Caption = MSHFlexGrid3.Rows - 1
    End If
End Sub

Private Sub Form_Load()
    DTPicker1.Value = Date$    '设置显示的日期
    DTPicker2.Value = Date$    '设置显示的日期
    Option1.Value = True
Exit Sub
    Dim rs As ADODB.Recordset
    Dim i As Integer
    Conn = App.Path & "\Data\测试数据.mdb"
    strDb = Conn
    Conn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
     strDb & ";Persist Security Info=False"                           '连接数据库
    Set rs = QueryExt("SELECT 产品编号 FROM 产品信息表")    '获取产品编号信息
    rs.MoveFirst
    With Combo1             '加载组合框选项
        .Clear
        Do While Not rs.EOF
            .AddItem rs("产品编号")
            rs.MoveNext
        Loop
        .ListIndex = 0
    End With
    rs.Close
    Set rs = QueryExt("SELECT 操作员 FROM 用户信息表")    '获取操作员信息
    With Combo2             '加载组合框选项
        .Clear
        Do While Not rs.EOF
            .AddItem rs("操作员")
            rs.MoveNext
        Loop
        .ListIndex = 0
    End With
    rs.Close
'    StatusBar1.Panels.Item(1) = "当前用户： " & strName1     '填写状态条第一栏
    DTPicker1.Value = Date$    '设置显示的日期
    DTPicker2.Value = Date$    '设置显示的日期
    Command3.TabIndex = 0
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Unload Me
    Main.Show
End Sub



