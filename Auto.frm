VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Auto 
   BackColor       =   &H8000000C&
   Caption         =   "CN115前悬挂外倾角检测自动模式"
   ClientHeight    =   12390
   ClientLeft      =   120
   ClientTop       =   1335
   ClientWidth     =   20625
   Icon            =   "Auto.frx":0000
   LinkMode        =   1  'Source
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   12390
   ScaleWidth      =   20625
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      BackColor       =   &H0080C0FF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   15.75
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   98
      Top             =   4320
      Width           =   5295
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Command6"
      Height          =   492
      Left            =   2040
      TabIndex        =   97
      Top             =   3360
      Width           =   1332
   End
   Begin VB.TextBox Text13 
      Height          =   492
      Left            =   960
      TabIndex        =   96
      Text            =   "Text13"
      Top             =   1440
      Width           =   2772
   End
   Begin VB.Timer Timer5 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   9960
      Top             =   1440
   End
   Begin VB.TextBox Text10 
      BackColor       =   &H0000FFFF&
      Height          =   612
      Left            =   1920
      TabIndex        =   90
      Text            =   "4w654654"
      Top             =   5040
      Width           =   972
   End
   Begin VB.Frame Frame8 
      Caption         =   "后台"
      Height          =   7932
      Left            =   12120
      TabIndex        =   8
      Top             =   5160
      Visible         =   0   'False
      Width           =   8292
      Begin VB.Timer Timer7 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   5040
         Top             =   4200
      End
      Begin VB.TextBox Text9 
         Height          =   495
         Left            =   3600
         TabIndex        =   83
         Text            =   "Text9"
         Top             =   5760
         Width           =   1575
      End
      Begin VB.TextBox Text8 
         Height          =   495
         Left            =   5160
         TabIndex        =   82
         Top             =   600
         Width           =   3015
      End
      Begin VB.Frame Frame1 
         Caption         =   "PLC通讯交互信号"
         Height          =   7455
         Index           =   0
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   3015
         Begin VB.CommandButton Command8 
            Caption         =   "Command8"
            Height          =   375
            Left            =   1560
            TabIndex        =   41
            Top             =   6000
            Width           =   735
         End
         Begin VB.TextBox Text7 
            Height          =   495
            Left            =   1920
            TabIndex        =   40
            Text            =   "Text7"
            Top             =   2640
            Width           =   855
         End
         Begin VB.TextBox Text6 
            Height          =   375
            Left            =   1920
            TabIndex        =   39
            Text            =   "Text6"
            Top             =   1680
            Width           =   735
         End
         Begin VB.TextBox Text5 
            Height          =   495
            Left            =   1920
            TabIndex        =   38
            Text            =   "Text5"
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox HoldValue 
            Height          =   375
            Index           =   0
            Left            =   840
            TabIndex        =   27
            Top             =   360
            Width           =   855
         End
         Begin VB.TextBox HoldValue 
            Height          =   375
            Index           =   1
            Left            =   840
            TabIndex        =   26
            Top             =   820
            Width           =   855
         End
         Begin VB.TextBox HoldValue 
            Height          =   375
            Index           =   2
            Left            =   840
            TabIndex        =   25
            Top             =   1280
            Width           =   855
         End
         Begin VB.TextBox HoldValue 
            Height          =   375
            Index           =   3
            Left            =   840
            TabIndex        =   24
            Top             =   1740
            Width           =   855
         End
         Begin VB.TextBox HoldValue 
            Height          =   375
            Index           =   4
            Left            =   840
            TabIndex        =   23
            Top             =   2200
            Width           =   855
         End
         Begin VB.TextBox HoldValue 
            Height          =   375
            Index           =   5
            Left            =   840
            TabIndex        =   22
            Top             =   2660
            Width           =   855
         End
         Begin VB.TextBox HoldValue 
            Height          =   375
            Index           =   6
            Left            =   840
            TabIndex        =   21
            Top             =   3120
            Width           =   855
         End
         Begin VB.TextBox PLCRx 
            Height          =   495
            Left            =   120
            TabIndex        =   20
            Top             =   3600
            Width           =   2055
         End
         Begin VB.CommandButton Command4 
            Caption         =   "连接"
            Height          =   375
            Left            =   120
            TabIndex        =   19
            Top             =   4320
            Width           =   735
         End
         Begin VB.CommandButton Command5 
            Caption         =   "断开"
            Height          =   375
            Left            =   960
            TabIndex        =   18
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox Text1 
            Height          =   375
            Left            =   120
            TabIndex        =   17
            Text            =   "00050500FF00"
            Top             =   5160
            Width           =   2175
         End
         Begin VB.CommandButton Command3 
            Caption         =   "停止Y0"
            Height          =   375
            Left            =   240
            TabIndex        =   16
            Top             =   6720
            Width           =   855
         End
         Begin VB.CommandButton Command1 
            Caption         =   "启动Y0"
            Height          =   495
            Left            =   120
            TabIndex        =   15
            Top             =   6000
            Width           =   1095
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Command2"
            Height          =   615
            Left            =   1440
            TabIndex        =   14
            Top             =   6600
            Width           =   855
         End
         Begin VB.Timer Timer3 
            Enabled         =   0   'False
            Interval        =   100
            Left            =   2040
            Top             =   960
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "D600"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.5
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   240
            TabIndex        =   34
            Top             =   442
            Width           =   480
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "D601"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.5
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   1
            Left            =   240
            TabIndex        =   33
            Top             =   902
            Width           =   480
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "D602"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.5
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   2
            Left            =   240
            TabIndex        =   32
            Top             =   1362
            Width           =   480
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "D603"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.5
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   3
            Left            =   240
            TabIndex        =   31
            Top             =   1822
            Width           =   480
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "D604"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.5
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   240
            TabIndex        =   30
            Top             =   2282
            Width           =   480
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "D605"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.5
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   5
            Left            =   240
            TabIndex        =   29
            Top             =   2742
            Width           =   480
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "D606"
            BeginProperty Font 
               Name            =   "宋体"
               Size            =   10.5
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   6
            Left            =   240
            TabIndex        =   28
            Top             =   3202
            Width           =   480
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "参数读取"
         Height          =   975
         Left            =   3360
         TabIndex        =   37
         Top             =   360
         Width           =   1455
         Begin VB.Timer Timer4 
            Enabled         =   0   'False
            Interval        =   1000
            Left            =   240
            Top             =   360
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "传感器通讯"
         Height          =   1575
         Left            =   3360
         TabIndex        =   11
         Top             =   3120
         Width           =   2295
         Begin VB.TextBox SensorText 
            Height          =   495
            Left            =   960
            TabIndex        =   12
            Top             =   360
            Width           =   1095
         End
         Begin VB.Timer Timer2 
            Interval        =   100
            Left            =   360
            Top             =   1080
         End
         Begin MSCommLib.MSComm MSComm2 
            Left            =   240
            Top             =   240
            _ExtentX        =   1005
            _ExtentY        =   1005
            _Version        =   393216
            DTREnable       =   -1  'True
            RThreshold      =   1
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "扫描枪通讯"
         Height          =   1455
         Left            =   3360
         TabIndex        =   9
         Top             =   1440
         Width           =   2295
         Begin VB.TextBox SybolStr 
            Height          =   495
            Left            =   1200
            TabIndex        =   10
            Top             =   360
            Width           =   855
         End
         Begin VB.Timer Timer1 
            Interval        =   100
            Left            =   360
            Top             =   960
         End
         Begin MSCommLib.MSComm MSComm1 
            Left            =   240
            Top             =   240
            _ExtentX        =   1005
            _ExtentY        =   1005
            _Version        =   393216
            CommPort        =   5
            DTREnable       =   -1  'True
            RThreshold      =   1
         End
      End
   End
   Begin VB.Frame Frame9 
      BackColor       =   &H00FFFF00&
      Caption         =   "PLC信号"
      Height          =   615
      Left            =   16320
      TabIndex        =   42
      Top             =   1320
      Visible         =   0   'False
      Width           =   2175
      Begin VB.Timer Timer6 
         Interval        =   100
         Left            =   720
         Top             =   0
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y21"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   25
         Left            =   3840
         TabIndex        =   74
         Top             =   720
         Width           =   405
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   27
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   2880
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   26
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   2520
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   25
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   2160
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   24
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   1800
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   23
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   1440
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   22
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   1080
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   21
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   720
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   20
         Left            =   4440
         Shape           =   2  'Oval
         Top             =   360
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   7
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   2880
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   6
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   2520
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   5
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   2160
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   4
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   1800
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   3
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   1440
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   2
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   1080
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   1
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   720
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   0
         Left            =   3120
         Shape           =   2  'Oval
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y27"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   31
         Left            =   3840
         TabIndex        =   73
         Top             =   2880
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y26"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   30
         Left            =   3840
         TabIndex        =   72
         Top             =   2520
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y25"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   29
         Left            =   3840
         TabIndex        =   71
         Top             =   2160
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y24"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   28
         Left            =   3840
         TabIndex        =   70
         Top             =   1800
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y23"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   27
         Left            =   3840
         TabIndex        =   69
         Top             =   1440
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y22"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   26
         Left            =   3840
         TabIndex        =   68
         Top             =   1080
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y20"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   24
         Left            =   3840
         TabIndex        =   67
         Top             =   360
         Width           =   405
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y7"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   23
         Left            =   2640
         TabIndex        =   66
         Top             =   2880
         Width           =   270
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y6"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   22
         Left            =   2640
         TabIndex        =   65
         Top             =   2520
         Width           =   270
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y5"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   21
         Left            =   2640
         TabIndex        =   64
         Top             =   2160
         Width           =   270
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y4"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   20
         Left            =   2640
         TabIndex        =   63
         Top             =   1800
         Width           =   270
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y3"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   19
         Left            =   2640
         TabIndex        =   62
         Top             =   1440
         Width           =   270
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y2"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   18
         Left            =   2640
         TabIndex        =   61
         Top             =   1080
         Width           =   270
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y1"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   17
         Left            =   2640
         TabIndex        =   60
         Top             =   720
         Width           =   270
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Y0"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   16
         Left            =   2640
         TabIndex        =   59
         Top             =   360
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   27
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   2880
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X27"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   15
         Left            =   1200
         TabIndex        =   58
         Top             =   2880
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   26
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   2520
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X26"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   14
         Left            =   1200
         TabIndex        =   57
         Top             =   2520
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   25
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   2160
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X25"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   13
         Left            =   1200
         TabIndex        =   56
         Top             =   2160
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   24
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   1800
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X24"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   12
         Left            =   1200
         TabIndex        =   55
         Top             =   1800
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   23
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   1440
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X23"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   11
         Left            =   1200
         TabIndex        =   54
         Top             =   1440
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   22
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X22"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   1200
         TabIndex        =   53
         Top             =   1080
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   21
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X21"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   9
         Left            =   1200
         TabIndex        =   52
         Top             =   720
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   20
         Left            =   1680
         Shape           =   2  'Oval
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X20"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   1200
         TabIndex        =   51
         Top             =   360
         Width           =   405
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   7
         Left            =   600
         Shape           =   2  'Oval
         Top             =   2880
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X7"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   7
         Left            =   240
         TabIndex        =   50
         Top             =   2880
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   6
         Left            =   600
         Shape           =   2  'Oval
         Top             =   2520
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X6"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   6
         Left            =   240
         TabIndex        =   49
         Top             =   2520
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   5
         Left            =   600
         Shape           =   2  'Oval
         Top             =   2160
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X5"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   240
         TabIndex        =   48
         Top             =   2160
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   4
         Left            =   600
         Shape           =   2  'Oval
         Top             =   1800
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X4"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   240
         TabIndex        =   47
         Top             =   1800
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   3
         Left            =   600
         Shape           =   2  'Oval
         Top             =   1440
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X3"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   3
         Left            =   240
         TabIndex        =   46
         Top             =   1440
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   2
         Left            =   600
         Shape           =   2  'Oval
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X2"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   240
         TabIndex        =   45
         Top             =   1080
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   1
         Left            =   600
         Shape           =   2  'Oval
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X1"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   240
         TabIndex        =   44
         Top             =   720
         Width           =   270
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   0
         Left            =   600
         Shape           =   2  'Oval
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "X0"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   43
         Top             =   360
         Width           =   270
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H0099AEA8&
      Caption         =   "检索条件"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3255
      Index           =   2
      Left            =   15360
      TabIndex        =   77
      Top             =   6720
      Visible         =   0   'False
      Width           =   4935
      Begin VB.Frame Frame1 
         BackColor       =   &H0099AEA8&
         Caption         =   "结束时间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   975
         Index           =   5
         Left            =   360
         TabIndex        =   80
         Top             =   1440
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker2 
            Height          =   375
            Left            =   720
            TabIndex        =   81
            Top             =   360
            Width           =   2415
            _ExtentX        =   4260
            _ExtentY        =   661
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   278986753
            CurrentDate     =   42568
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H0099AEA8&
         Caption         =   "起始时间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   975
         Index           =   4
         Left            =   360
         TabIndex        =   78
         Top             =   480
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   375
            Left            =   720
            TabIndex        =   79
            Top             =   360
            Width           =   2415
            _ExtentX        =   4260
            _ExtentY        =   661
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   278986753
            CurrentDate     =   42370
         End
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00808080&
      Caption         =   "今日生产情况"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5055
      Left            =   0
      TabIndex        =   75
      Top             =   6120
      Width           =   11535
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
         Height          =   4695
         Left            =   240
         TabIndex        =   76
         Top             =   240
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   8281
         _Version        =   393216
         BackColor       =   8421504
         BackColorFixed  =   16711680
         BackColorBkg    =   8421504
         ScrollTrack     =   -1  'True
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
   End
   Begin VB.Frame Frame17 
      BackColor       =   &H00808080&
      Caption         =   "实时信息"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5055
      Left            =   11600
      TabIndex        =   35
      Top             =   6120
      Width           =   3615
      Begin VB.TextBox Text2 
         BackColor       =   &H00808080&
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   21.75
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   4815
         Left            =   60
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   36
         Top             =   300
         Width           =   3495
      End
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00808080&
      Caption         =   "检测结果"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000B&
      Height          =   5172
      Left            =   5640
      TabIndex        =   2
      Top             =   720
      Width           =   8052
      Begin VB.TextBox Text4 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF00&
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   18
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Left            =   1656
         TabIndex        =   93
         Text            =   "待检测"
         Top             =   1440
         Width           =   1452
      End
      Begin VB.TextBox Text12 
         Alignment       =   2  'Center
         BackColor       =   &H0080C0FF&
         Height          =   372
         Left            =   1680
         TabIndex        =   92
         Text            =   "Text4"
         Top             =   480
         Width           =   2412
      End
      Begin VB.TextBox Text11 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF00&
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   18
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Left            =   1680
         TabIndex        =   91
         Text            =   "待检测"
         Top             =   2280
         Width           =   1452
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H0080C0FF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   15.75
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1560
         TabIndex        =   7
         Top             =   3228
         Width           =   5295
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "待检测"
         BeginProperty Font 
            Name            =   "微软雅黑"
            Size            =   22.5
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFF00&
         Height          =   600
         Left            =   1080
         TabIndex        =   99
         Top             =   4080
         Width           =   1332
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "选型匹配："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   240
         TabIndex        =   95
         Top             =   2400
         Width           =   1260
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "重码检测："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   216
         TabIndex        =   94
         Top             =   1560
         Width           =   1260
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "待检测"
         BeginProperty Font 
            Name            =   "微软雅黑"
            Size            =   42
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFF00&
         Height          =   1116
         Left            =   4080
         TabIndex        =   89
         Top             =   1440
         Width           =   2520
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   14.25
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   19
         Left            =   1560
         TabIndex        =   88
         Top             =   2160
         Width           =   165
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   348
         Index           =   17
         Left            =   72
         TabIndex        =   87
         Top             =   2184
         Width           =   972
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "是否重码："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   16
         Left            =   9402
         TabIndex        =   86
         Top             =   1392
         Width           =   1260
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   14.25
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   15
         Left            =   1560
         TabIndex        =   85
         Top             =   585
         Width           =   165
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   14.25
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   288
         Index           =   14
         Left            =   1560
         TabIndex        =   84
         Top             =   3744
         Width           =   156
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "扫描条码："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   240
         TabIndex        =   6
         Top             =   3360
         Width           =   1272
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   240
         TabIndex        =   5
         Top             =   3768
         Width           =   132
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "产品型号："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   3
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   1275
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "符合所选产品："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   9462
         TabIndex        =   3
         Top             =   2376
         Width           =   1764
      End
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   16440
      Top             =   840
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "2018/12/14 14:38:29"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   12240
      TabIndex        =   1
      Top             =   480
      Width           =   2565
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "CN115前悬挂外倾角检测系统"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   21.75
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   5280
      TabIndex        =   0
      Top             =   120
      Width           =   5700
   End
End
Attribute VB_Name = "Auto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'sOpton Explicit
' Call Windows API (For winsock use)
Private Declare Function inet_addr Lib "wsock32.dll" (ByVal s As String) As Long
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
' Call Window API (For registry use, Find Serial Port list)
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function timeGetTime Lib "winmm.dll" () As Long

Dim ReceiveCount1 As Long                                                            ' 接收数据字节计数器（条码）
Dim InputSignal1 As String                                                           ' 接收缓冲暂存
Dim DisplaySwitch1 As Boolean                                                        ' 显示开关

Dim ReceiveCount2 As Long                                                            ' 接收数据字节计数器（PLC）
Dim InputSignal2 As String                                                           ' 接收缓冲暂存(PLC)
Dim DisplaySwitch2 As Boolean                                                        ' 显示开关

Dim SensorTemp As String
Dim PARA(1 To 9) As String
Dim ProductMax As String                                                              '最大值
Dim ProductMin As String                                                              '最小值
Dim isExist As Boolean '是否已近存在条码
Dim isMatch As Boolean '产品是否匹配当前场景
Dim strDb As String
Dim TAG1 As Integer

Const REG_SZ = 1
Const HKEY_LOCAL_MACHINE = &H80000002
Const ERROR_SUCCESS = 0&
Const SYNCHRONIZE = &H100000
Const STANDARD_RIGHTS_READ = &H20000
Const KEY_QUERY_VALUE = &H1
Const KEY_CREATE_SUB_KEY = &H4
Const KEY_ENUMERATE_SUB_KEYS = &H8
Const KEY_NOTIFY = &H10
Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))

'Convert HEX to ASCII
Private Sub HEX_to_ASCI(ByVal value_hex As Byte, ByRef asci() As Byte)
    Dim reglow, reghigh As Byte
    reghigh = (value_hex And &HF0) / 16   ' >> 4
    reglow = value_hex And &HF
    If (reghigh <= 9) Then
        asci(0) = reghigh + &H30
    Else
        asci(0) = (reghigh - 10) + &H41
    End If
    
    If (reglow <= 9) Then
        asci(1) = reglow + &H30
    Else
        asci(1) = (reglow - 10) + &H41
    End If
End Sub


Public Function DEC_to_BIN(Dec As Long) As String
Dim i As Integer
    DEC_to_BIN = ""
    Do While Dec > 0
        DEC_to_BIN = Dec Mod 2 & DEC_to_BIN
        Dec = Dec \ 2
    Loop
    If Len(DEC_to_BIN) < 16 Then
        For i = 1 To 16 - Len(DEC_to_BIN)
            DEC_to_BIN = "0" & DEC_to_BIN
        Next i
    End If
End Function

Private Sub waittime(delay As Single)
    Dim starttime As Single
    starttime = timeGetTime
    While timeGetTime < starttime + (delay * 1000)
    DoEvents
    Wend
End Sub


Public Function HEX_to_DEC(ByVal Hex As String) As Long
   Dim i As Long
   Dim b As Long
On Error GoTo err
   Hex = UCase(Hex)
   For i = 1 To Len(Hex)
       Select Case Mid(Hex, Len(Hex) - i + 1, 1)
           Case "0": b = b + 16 ^ (i - 1) * 0
           Case "1": b = b + 16 ^ (i - 1) * 1
           Case "2": b = b + 16 ^ (i - 1) * 2
           Case "3": b = b + 16 ^ (i - 1) * 3
           Case "4": b = b + 16 ^ (i - 1) * 4
           Case "5": b = b + 16 ^ (i - 1) * 5
           Case "6": b = b + 16 ^ (i - 1) * 6
           Case "7": b = b + 16 ^ (i - 1) * 7
           Case "8": b = b + 16 ^ (i - 1) * 8
           Case "9": b = b + 16 ^ (i - 1) * 9
           Case "A": b = b + 16 ^ (i - 1) * 10
           Case "B": b = b + 16 ^ (i - 1) * 11
           Case "C": b = b + 16 ^ (i - 1) * 12
           Case "D": b = b + 16 ^ (i - 1) * 13
           Case "E": b = b + 16 ^ (i - 1) * 14
           Case "F": b = b + 16 ^ (i - 1) * 15
       End Select
   Next i
   HEX_to_DEC = b
err:
End Function

Private Sub JX1(A As String)  'PLC上传数据解析
    Dim TNm(0 To 14) As String
    If Len(A) = 43 Then
         TNm(0) = HEX_to_DEC(Mid(A, 10, 2))
         TNm(1) = HEX_to_DEC(Mid(A, 18, 2))
         TNm(2) = HEX_to_DEC(Mid(A, 26, 2)) 'D604
         
         Text3.Text = TNm(0)
         Model = TNm(1)
         Text1.Text = TNm(2)
         Text9.Text = DEC_to_BIN(Text1.Text)
'        TNm(0) = formatdata(HEX_to_DEC5(Mid(a, 12, 4) & Mid(a, 8, 4)))
'        TNm(1) = formatdata(HEX_to_DEC5(Mid(a, 20, 4) & Mid(a, 16, 4)))
'
'        TNm(8) = Chr(HEX_to_DEC5(Mid(a, 74, 2))) & Chr(HEX_to_DEC5(Mid(a, 72, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 78, 2))) & Chr(HEX_to_DEC5(Mid(a, 76, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 82, 2))) & Chr(HEX_to_DEC5(Mid(a, 80, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 86, 2))) & Chr(HEX_to_DEC5(Mid(a, 84, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 90, 2))) & Chr(HEX_to_DEC5(Mid(a, 88, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 94, 2))) & Chr(HEX_to_DEC5(Mid(a, 92, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 98, 2))) & Chr(HEX_to_DEC5(Mid(a, 96, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 102, 2))) & Chr(HEX_to_DEC5(Mid(a, 100, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 106, 2))) & Chr(HEX_to_DEC5(Mid(a, 104, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 110, 2))) & Chr(HEX_to_DEC5(Mid(a, 108, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 114, 2))) & Chr(HEX_to_DEC5(Mid(a, 112, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 118, 2))) & Chr(HEX_to_DEC5(Mid(a, 116, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 122, 2))) & Chr(HEX_to_DEC5(Mid(a, 120, 2))) _
'                & Chr(HEX_to_DEC5(Mid(a, 126, 2)))
'        TNm(9) = HEX_to_DEC5(Mid(a, 130, 2))
'        TNm(10) = HEX_to_DEC5(Mid(a, 134, 2))
    Else
        Text3.Text = ""
    End If
End Sub



Private Sub Command6_Click()
MSComm2.Output = ":01050B00FF00F0" & Chr(13) & Chr(10)
End Sub

'timer1 扫码枪
'timer2 连接串口，读取传感器的值
'timer3 连接DMT模块
'timer4 读取设置
Private Sub Form_Load()
    TAG1 = 0
    Dim strDb As String
    Dim PLCcon As Integer
    Timer5.Enabled = True
    Timer2.Enabled = True
    Timer7.Enabled = True
'    Auto.Width = 15600
'    Auto.Height = 11760
'    Timer4.Enabled = True
''    Text10.SetFocus
'    DTPicker1.Value = Date$    '设置显示的日期
'    DTPicker2.Value = Date$    '设置显示的日期
'    Conn = App.Path & "\Data\测试数据.mdb"
'    strDb = Conn
'    Conn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
'    strDb & ";Persist Security Info=False"                            '连接数据库
    Call Comm_initial(3, 9600, "N", 8, 1, MSComm1) 'PLC通讯
    Call Comm_initial(5, 9600, "N", 8, 1, MSComm2) '扫码枪通讯
 
'    Timer4.Enabled = True
'CONNNN:
'    PLCcon = OpenModbusTCPSocket(0, inet_addr("192.168.1.5"), 502) '连接MODBUSTCP
'    If PLCcon <> -1 Then     '结果为-1连接失败，定时器重连
'        Timer3.Enabled = True
'    Else
'        GoTo CONNNN
'    End If
'    Command9_Click
End Sub

Private Sub Comm_initial(port As Byte, BaudRate As String, ParityBit As String, DataBit As Integer, StopBit As Integer, ms As MSComm)
    On Error GoTo ErrorTrap                                                   ' 错误则跳往错误处理
    If ms.PortOpen = True Then ms.PortOpen = False                            ' 先判断串口是否打开，如果打开则先关闭
     ms.CommPort = port                                                       ' 设定端口
     ms.Settings = BaudRate & "," & ParityBit & "," & DataBit & "," & StopBit ' 设置波特率，无校验，8位数据位，1位停止位
     ms.InBufferSize = 1024                                                   ' 设置接收缓冲区为1024字节
     ms.OutBufferSize = 4096                                                  ' 设置发送缓冲区为4096字节
     ms.InBufferCount = 0                                                     ' 清空输入缓冲区
     ms.OutBufferCount = 0                                                    ' 清空输出缓冲区
     ms.SThreshold = 1                                                        ' 发送缓冲区空触发发送事件
     ms.RThreshold = 1                                                        ' 每X个字符到接收缓冲区引起触发接收事件
     ms.OutBufferCount = 0                                                    ' 清空发送缓冲区
     ms.InBufferCount = 0                                                     ' 滑空接收缓冲
     ms.PortOpen = True                                                       'x 打开串口
     Exit Sub
ErrorTrap:                                                                    ' 错误处理
End Sub





'窗口关闭事件
Private Sub Form_Unload(Cancel As Integer)
    Timer2.Enabled = False
    Timer3.Enabled = False
    Timer4.Enabled = False
    Call CloseSocket(0)
    If MSComm1.PortOpen = True Then
        MSComm1.PortOpen = False
    End If
    If MSComm2.PortOpen = True Then
        MSComm2.PortOpen = False
    End If
End Sub

Private Sub Label1_Click()
   ' Command7_Click
End Sub



Private Function Release() As Boolean
MSComm2.Output = ":01050B00FF00F0" & Chr(13) & Chr(10)
End Function

Private Sub MSComm1_OnComm()
On Error GoTo err
    Select Case MSComm1.CommEvent ' 每接收1个数就触发一次
        Case comEvReceive
        Call SybolStrRx             ' 条码接收
        Case comEvSend            ' 每发送1个数就触发一次
        Case Else
    End Select
err:
End Sub


Private Sub MSComm2_OnComm()
On Error GoTo err
    Select Case MSComm2.CommEvent ' 每接收1个数就触发一次
        Case comEvReceive
        Call PLCStrRx             ' 条码接收
        Case comEvSend            ' 每发送1个数就触发一次
        Case Else
    End Select
err:
End Sub



'====================================================================================
'                PLC接收()
'====================================================================================
Private Sub PLCStrRx()
On Error GoTo err
    InputSignal2 = MSComm2.Input
    ReceiveCount2 = ReceiveCount2 + LenB(StrConv(InputSignal2, vbFromUnicode))     ' 计算总接收数据
                                                                                   ' 显示接收文本
        SensorText.Text = SensorText.Text & InputSignal2                           '
        SensorText.SelStart = Len(SensorText.Text)                                 ' 显示光标位置
err:
End Sub





'====================================================================================
'                条码接收()
'====================================================================================
Private Sub SybolStrRx()
On Error GoTo err
    InputSignal1 = MSComm1.Input
    ReceiveCount1 = ReceiveCount1 + LenB(StrConv(InputSignal1, vbFromUnicode)) ' 计算总接收数据
    If DisplaySwitch1 = False Then                                             ' 显示接收文本
        SybolStr.Text = SybolStr.Text & InputSignal1                           '
        SybolStr.SelStart = Len(SybolStr.Text)                                 ' 显示光标位置
    End If
err:
End Sub




Private Sub Timer1_Timer()
    If SybolStr.Text <> "" Then
        waittime (0.05)
        Text3.Text = SybolStr.Text
        SybolStr.Text = ""
         Text12.Text = GetVehicleType(Text3.Text)
        If FormatChack(Text3.Text) = True Then
            '查询数据库isExist
             isExist = Exist()
             isMatch = Match("测试车型", GetVehicleType(Text3.Text))
             If isExist = True And idMatch = True Then
                 Label8.Caption = "合格"
                 Label8.ForeColor = &HFF00&
                 TAG1 = 1 '将标志位置1，告诉线体放行
                 '写入数据库
                 Call Savedata(Text3.Text, "宝骏510", "合格")
             Else:
               Label8.Caption = "不合格"
               Call Savedata(Text3.Text, "宝骏510", "不合格")
                 Label8.ForeColor = &HFF&
             End If
       Else: Label8.Caption = "条码错误"
       Label8.ForeColor = &HFFFF&
       End If
     
    End If
End Sub


Private Sub Timer2_Timer()
If SensorText.Text <> "" Then
        waittime (0.05)
        Dim res As String
        res = SensorText.Text
        Text14.Text = res
        If res = ":01050B00FF00F0" & Chr(13) & Chr(10) Then
        Label3.Caption = ""
        
        Text10.Text = "True"
        Else: Text10.Text = "false"
        End If
    End If
End Sub


'查询结果为空返回true，不为空返回false
Private Function Exist() As Boolean
    Dim rs As ADODB.Recordset
    Dim str() As String, str1 As String
    Dim i As Long
    Dim number As Double, Number1 As Double, Number2 As Double, Number3 As Double, Number4 As Double, Number5 As Double, number6 As Double, number7 As Double
    Dim SQL As String, t As String
    Dim Date1 As Date, Date2 As Date

    Conn = App.Path & "\Data\DB_Code.mdb"
    strDb = Conn
    Conn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
     strDb & ";Persist Security Info=False"
    t = Conn
    Adodc1.ConnectionString = Conn
    Adodc1.CommandType = adCmdText
     Adodc1.RecordSource = "SELECT * FROM  Table1 WHERE Code= '" & Text3.Text & "'"
    Adodc1.Refresh
   If Adodc1.Recordset.RecordCount = 0 Then
   Exist = True
   Text4.Text = "通过"
   Text4.BackColor = &HFF00&
   Else
   Exist = False
   Text4.Text = "不通过"
   Text4.BackColor = &HFF&
   
   End If
    Set MSHFlexGrid1.DataSource = Adodc1
    MSHFlexGrid1.Refresh
    With MSHFlexGrid1
      
    End With
End Function


'产品选型匹配
Private Function Match(vType As String, vCode As String) As Boolean
    Dim rs As ADODB.Recordset
    Dim str() As String, str1 As String
    Dim i As Long
    Dim number As Double, Number1 As Double, Number2 As Double, Number3 As Double, Number4 As Double, Number5 As Double, number6 As Double, number7 As Double
    Dim SQL As String, t As String
    Dim Date1 As Date, Date2 As Date

    Conn = App.Path & "\Data\DB_Code.mdb"
    strDb = Conn
    Conn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
     strDb & ";Persist Security Info=False"
    t = Conn
    Adodc1.ConnectionString = Conn
    Adodc1.CommandType = adCmdText
     Adodc1.RecordSource = "SELECT * FROM  Table2  WHERE ComponentsCode= '" & vCode & "'"
    Adodc1.Refresh
    
    ' Text13.Text = Adodc1.Recordset.Filter = "ComponentType="
    Text13.Text = Adodc1.Recordset.Fields("ComponentType")
    If Adodc1.Recordset.Fields("ComponentType") = vType Then
    Match = True
    
    Text11.Text = "通过"
    Text11.BackColor = &HFF00&
    
     Else: Match = False
        Text11.Text = "不通过"
        Text11.BackColor = &HFF&
     End If
End Function


'获取车型
Private Function GetVehicleType(iCode As String) As String
Dim Lcode As String
Lcode = Mid(iCode, 11, 8)
GetVehicleType = Lcode
End Function





'重置检测结果标签
Private Sub Timer5_Timer()
Text4.Text = "待检测"
Text4.BackColor = &HFFFF00
Text11.Text = "待检测"
Text11.BackColor = &HFFFF00
Label8.Caption = "待检测"
Label8.ForeColor = &HFFFF00
Text3.Text = ""
End Sub



Private Sub Timer7_Timer()
If TAG1 = 1 Then
MSComm2.Output = ":01050B00FF00F0" & Chr(13) & Chr(10) '扫描枪
TAG1 = 0
Else:
MSComm2.Output = ":0103125800088A" & Chr(13) & Chr(10) '读取读D600~D607
End If
End Sub


'条码正确性检测
Private Function FormatChack(SybolStr As String) As Boolean
       Dim SybolOK As Integer
       Dim SybolNG As Integer
       
        For i = 1 To Len(SybolStr)
            If Asc(Mid(SybolStr, i, 1)) >= 48 And Asc(Mid(SybolStr, i, 1)) <= 57 Or _
                Asc(Mid(SybolStr, i, 1)) >= 65 And Asc(Mid(SybolStr, i, 1)) <= 90 Or _
                Asc(Mid(SybolStr, i, 1)) >= 97 And Asc(Mid(SybolStr, i, 1)) <= 122 _
            Then
                SybolOK = SybolOK + 1
            Else
                SybolNG = SybolNG + 1
            End If
        Next i
        If Len(SybolStr) = SybolOK And SybolNG = 0 And Len(SybolStr) = 27 Then
            FormatChack = True
        Else
            FormatChack = False
        End If
End Function

Private Sub Savedata(code As String, vehicleType As String, res As String) '数据保存
    Dim rs As ADODB.Recordset
    Dim SQL As String
      Set rs = QueryExt("select * from Table1")
        rs.AddNew                     '添加
        rs("Code") = code '条形码
        rs("Time") = Format(Now, "yyyy-mm-dd hh:mm:ss") '日期和时间
        rs("VehicleType") = vehicleType '车型
        rs("Res") = res
        rs.Update
        rs.Close
End Sub





