VERSION 5.00
Begin VB.Form Opt 
   Caption         =   "高级设置"
   ClientHeight    =   6996
   ClientLeft      =   120
   ClientTop       =   456
   ClientWidth     =   9072
   LinkTopic       =   "Form1"
   ScaleHeight     =   6996
   ScaleWidth      =   9072
   StartUpPosition =   3  '窗口缺省
   Begin VB.TextBox Text10 
      Height          =   375
      Left            =   6480
      TabIndex        =   22
      Text            =   "Text10"
      Top             =   5520
      Width           =   1575
   End
   Begin VB.CommandButton Command3 
      Caption         =   "标定件校准"
      Height          =   495
      Left            =   6600
      TabIndex        =   21
      Top             =   4320
      Width           =   1935
   End
   Begin VB.CommandButton Command2 
      Caption         =   "一键还原预设值"
      Height          =   495
      Left            =   4560
      TabIndex        =   20
      Top             =   4320
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "确认修改"
      Height          =   615
      Left            =   2760
      TabIndex        =   17
      Top             =   5760
      Width           =   3135
   End
   Begin VB.TextBox Text9 
      Height          =   375
      Left            =   2160
      TabIndex        =   16
      Top             =   4320
      Width           =   1935
   End
   Begin VB.TextBox Text8 
      Height          =   375
      Left            =   6360
      TabIndex        =   14
      Top             =   3360
      Width           =   1935
   End
   Begin VB.TextBox Text7 
      Height          =   375
      Left            =   6360
      TabIndex        =   12
      Top             =   2400
      Width           =   1935
   End
   Begin VB.TextBox Text6 
      Height          =   375
      Left            =   6360
      TabIndex        =   10
      Top             =   1440
      Width           =   1935
   End
   Begin VB.TextBox Text5 
      Height          =   375
      Left            =   6360
      TabIndex        =   8
      Top             =   600
      Width           =   1935
   End
   Begin VB.TextBox Text4 
      Height          =   375
      Left            =   2160
      TabIndex        =   7
      Top             =   3360
      Width           =   1935
   End
   Begin VB.TextBox Text3 
      Height          =   375
      Left            =   2160
      TabIndex        =   5
      Top             =   2400
      Width           =   1935
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   2160
      TabIndex        =   3
      Top             =   1440
      Width           =   1935
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   2160
      TabIndex        =   1
      Top             =   600
      Width           =   1935
   End
   Begin VB.PictureBox Picture1 
      Height          =   0
      Left            =   0
      ScaleHeight     =   0
      ScaleWidth      =   0
      TabIndex        =   19
      Top             =   0
      Width           =   0
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "CN113R最大角度:"
      Height          =   180
      Left            =   240
      TabIndex        =   18
      Top             =   4440
      Width           =   1350
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "检具角度值 "
      Height          =   180
      Index           =   4
      Left            =   5040
      TabIndex        =   15
      Top             =   720
      Width           =   990
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "CN210R最小角度"
      Height          =   180
      Index           =   3
      Left            =   4800
      TabIndex        =   13
      Top             =   3480
      Width           =   1260
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "CN210R最大角度"
      Height          =   180
      Left            =   4800
      TabIndex        =   11
      Top             =   2520
      Width           =   1260
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "CN113R最小角度"
      Height          =   180
      Left            =   4800
      TabIndex        =   9
      Top             =   1560
      Width           =   1260
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "传感器安装距离:"
      Height          =   180
      Index           =   1
      Left            =   240
      TabIndex        =   6
      Top             =   3480
      Width           =   1350
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "传感器3校正距离:"
      Height          =   180
      Left            =   240
      TabIndex        =   4
      Top             =   2520
      Width           =   1440
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "传感器2校正距离:"
      Height          =   180
      Left            =   240
      TabIndex        =   2
      Top             =   1560
      Width           =   1440
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "传感器1校正距离:"
      Height          =   180
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   1440
   End
End
Attribute VB_Name = "Opt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

    MsgBox "确定修改?", vbOKOnly, "系统提示"
Open App.Path & "\system\data.ini" For Output As #1
Print #1, "传感器1校正距离=" & Text1.Text
Print #1, "传感器2校正距离=" & Text2.Text
Print #1, "传感器3校正距离=" & Text3.Text
Print #1, "传感器安装距离=" & Text4.Text
Print #1, "检具角度值=" & Text5.Text
Print #1, "CN113R最大角度=" & Text6.Text
Print #1, "CN113R最小角度=" & Text7.Text
Print #1, "CN210R最大角度=" & Text8.Text
Print #1, "CN210R最小角度=" & Text9.Text
Close #1
MsgBox "修改成功"
End Sub
'一键还原
Private Sub Command2_Click()

      '  MsgBox "确定要还原设置吗?", vbOKOnly, "系统提示"
      
   Text1.Text = "+15.656"
   Text2.Text = "+15.888"
   Text3.Text = "+26.954"
   Text4.Text = 90
   Text5.Text = 4.42
   Text6.Text = 8.05
   Text7.Text = 7.45
   Text8.Text = "4.70"
   Text9.Text = 4.05
   
   Call Command1_Click
End Sub


Private Sub Form_Load()

 Dim PARA(1 To 9) As String
    Dim a1(1 To 10) As String
    Dim b1(1 To 9) As String
    Dim d As Integer
    Dim r As Integer
    Dim s As Integer
    Dim St As String
    d = 1
    
    '读取参数设置
    
    Open App.Path & "\system\data.ini" For Input As #1
    Do While Not EOF(1)
    Line Input #1, St
    'Debug.Print Str
    a1(d) = St
    'Print a(i)
    If d <= 9 Then
        d = d + 1
    End If
    Loop
    Close 1
    
    For s = 1 To 9
    'b = InStr(a(s), "=")
    b1(s) = Mid(a1(s), InStr(a1(s), "=") + 1)
    Next
    
    For r = 1 To 9
        PARA(r) = b1(r)
'        Text2.Text = Text2.Text & vbCrLf & PARA(r)
'        Text2.SelStart = Len(Text2.Text)
    Next

   Text1.Text = PARA(1)
   Text2.Text = PARA(2)
   Text3.Text = PARA(3)
   Text4.Text = PARA(4)
   Text5.Text = PARA(5)
   Text6.Text = PARA(6)
   Text7.Text = PARA(7)
   Text8.Text = PARA(8)
   Text9.Text = PARA(9)
   
   
End Sub




