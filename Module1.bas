Attribute VB_Name = "Module1"
  Option Explicit
Public cnn As ADODB.Connection
Public Conn As String
Public strDB As String
Dim Connect_Num As Long
Dim IsConnect As Boolean
Dim CONNECT_LOOP_MAX As Long
Public strName1 As String   '操作员
Public strName2 As String   '产品编号
Public xuanze As Long
Public xz As Boolean
Public bool3(6) As Boolean
Public ZiDong As Boolean
Public Boolxiezai As Boolean
Public gq As Integer
Public cp As Integer
Public ms As Integer
Public fmq As Integer '蜂鸣器报警
Public yy As Integer '语言状态
Public shouchiyun As Integer
Public chanshu(1 To 9) As Double
Public Declare Function GetTickCount Lib "kernel32" () As Long
Public Declare Function ShellAbout Lib "shell32.dll" Alias "ShellAboutA" (ByVal hwnd As Long, ByVal szApp As String, ByVal szOtherStuff As String, ByVal hIcon As Long) As Long
Private Sub Main()
    App.Title = "制动轮缸性能测试系统"  '系统标题
    Form1.Show   '显示登录窗体
End Sub

Private Sub Connect() '连接数据库
    If IsConnect = True Then  '如果连接标记为真，则返回。否则会出错
        Exit Sub
    End If
    Set cnn = New ADODB.Connection  '关键New用于创建新对象cnn
    cnn.ConnectionString = Conn  '设置连接字符串ConnectionString属性
    cnn.Open  '打开到数据库的连接
    If cnn.State <> adStateOpen Then  '判断连接的状态
        MsgBox "数据库连接失败", vbOKOnly + vbCritical, "警告"    '如果连接不成功，则显示提示信息，退出程序
        End
    End If
    
    IsConnect = True  '设置连接标记，表示已经连接到数据库
End Sub

Public Sub Disconnect() '断开与数据库的连接
    Dim rc As Long
    If IsConnect = False Then Exit Sub '如果连接标记为假，标明已经断开连接，则直接返回
    cnn.Close  '关闭连接
    Set cnn = Nothing
    IsConnect = False
End Sub

Public Sub DB_Connect() '使用Connect_Num控制数据库连接
    Connect_Num = Connect_Num + 1
    Connect
End Sub
Public Sub DB_Disconnect()
    If Connect_Num >= CONNECT_LOOP_MAX Then
        Connect_Num = 0
        Disconnect
    End If
End Sub
Public Sub DBapi_Disconnect() '强制关闭api方式访问的数据库，计数器复位
    Connect_Num = 0
    Disconnect
End Sub

Public Sub SQLExt(ByVal TmpSQLstmt As String)   '执行数据库操作语句
    Dim cmd As New ADODB.Command  '创建Command对象cmd
    DB_Connect    '连接到数据库
    Set cmd.ActiveConnection = cnn  '设置cmd的ActiveConnection属性，指定与其关联的数据库连接
    cmd.CommandText = TmpSQLstmt  '设置要执行的命令文本
    cmd.Execute
    Set cmd = Nothing
    DB_Disconnect
End Sub

Public Function QueryExt(ByVal TmpSQLstmt As String) As ADODB.Recordset '执行数据库查询语句
    Dim rst As New ADODB.Recordset
    DB_Connect    '连接到数据库
    Dim t
    Set rst.ActiveConnection = cnn    '设置rst的ActiveConnection属性，指定与其关联的数据库连接
    rst.CursorType = adOpenKeyset
    rst.LockType = adLockOptimistic   '设置锁定类型
    t = TmpSQLstmt
    rst.Open TmpSQLstmt    '打开记录集
    Set QueryExt = rst    '返回记录集
End Function



